import React, { useState } from "react"
import FormLogin from "./FormLogin"
import FormRegister from "./FormRegister"

const Forms = () => {
    const [displayRegister, setDisplayRegister] = useState(false)

    const changeFormHandler = () => {
        setDisplayRegister(!displayRegister)
    }

    return (
        <div className="container">
            <h4>
                <span>My</span>Snippets
            </h4>
            <h5>
                {displayRegister
                    ? "Create an account."
                    : "Sign in to your account."}
            </h5>

            {displayRegister ? (
                <FormRegister handler={changeFormHandler} />
            ) : (
                <FormLogin />
            )}

            <div className="dnthave" onClick={() => changeFormHandler()}>
                {displayRegister
                    ? "Already have an account ?"
                    : "Don’t have an account? Sign up"}
            </div>
        </div>
    )
}

export default Forms
