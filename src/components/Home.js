import React, { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { isLogin } from "../services/actions/auth.action"
import axios from "../services/axios"
import { isNotEmpty, isEmpty } from "../services/utils"
import { getAllNotes } from "../services/actions/note.action"
import { getUser } from "../services/actions/user.action"

const Home = () => {
    const [oneNote, setOneNote] = useState(null)
    const [title, setTitle] = useState("")
    const [content, setContent] = useState("")
    const [noteId, setNoteId] = useState("")

    const userId = localStorage.getItem("userId")

    const dispatch = useDispatch()
    const user = useSelector((state) => state.userReducer)
    const notes = useSelector((state) => state.noteReducer)

    useEffect(() => {
        dispatch(getUser())
        dispatch(getAllNotes())
    }, [oneNote])

    const showOne = (note) => {
        setOneNote(note)
        setTitle(note.title)
        setContent(note.content)
        setNoteId(note._id)
    }

    const clearState = () => {
        setTitle("")
        setContent("")
        setNoteId("")
    }

    const handleLogout = (e) => {
        e.preventDefault()
        axios
            .get("/user/logout")
            .then((res) => {
                localStorage.clear()
                dispatch(isLogin(false))
                // TODO vider les stores
            })
            .catch((err) => {
                console.log(err)
            })
    }

    const handleDelete = (e) => {
        e.preventDefault()
        deleteNote()
    }

    const deleteNote = () => {
        setOneNote(null)
        axios
            .delete(`/user/${userId}/note/${noteId}`)
            .then((res) => {
                console.log(res)
            })
            .catch((err) => {
                console.log(err)
            })
    }

    const handleBackOrNew = (e) => {
        e.preventDefault()

        // Back
        if (oneNote) {
            if (isEmpty(title) && isEmpty(content)) {
                deleteNote()
            } else {
                setOneNote(null)
                axios
                    .put(`/user/${userId}/note/${noteId}`, {
                        title,
                        content,
                    })
                    .then((res) => {
                        console.log(res)
                    })
                    .catch((err) => {
                        console.log(err)
                    })
            }
        } else {
            // New
            setOneNote({})
            clearState()
            let userId = localStorage.getItem("userId")
            axios
                .post(`/user/${userId}/note`, {
                    title: "",
                    content: "",
                })
                .then((res) => {
                    console.log(res)
                    setNoteId(res.data._id)
                })
                .catch((err) => {
                    console.log(err)
                })
        }
    }

    return (
        <div className="container-snip">
            <div className="container-head">
                <div className="container__head">
                    <div className="head-avatar">
                        <img src="img/avatar.svg" alt="avatar" />
                    </div>
                    <div className="head-login">{user.username}</div>
                </div>
                <div className="container__logout">
                    <input
                        type="submit"
                        value="Logout"
                        className="btn__logout"
                        onClick={(e) => handleLogout(e)}
                    />
                </div>
            </div>
            <div className="container__back">
                <div onClick={(e) => handleBackOrNew(e)} className="btn__back">
                    {oneNote ? "Back" : "New"}
                </div>
                {oneNote && (
                    <div onClick={(e) => handleDelete(e)} className="btn__back">
                        Delete
                    </div>
                )}
            </div>
            {oneNote === null ? (
                <div className="container__all__cards">
                    {isNotEmpty(notes) &&
                        notes.map((note) => (
                            <div
                                key={note._id}
                                className="container__card"
                                onClick={() => showOne(note)}
                            >
                                <div className="card__title">{note.title}</div>
                                <div className="card__content">
                                    {note.content}
                                </div>
                            </div>
                        ))}
                </div>
            ) : (
                /** component show one ou il y aura CRUD d'un note */
                <div className="container__show__note">
                    <input
                        className="container__show__note__title"
                        type="text"
                        name="title"
                        placeholder="Enter title"
                        id="pwd2"
                        autoComplete="off"
                        value={title}
                        onChange={(e) => setTitle(e.target.value)}
                    />
                    <textarea
                        className="container__show__note__content"
                        placeholder="Some content for this note..."
                        name="content"
                        rows="5"
                        onChange={(e) => setContent(e.target.value)}
                        defaultValue={content}
                    />
                </div>
            )}
        </div>
    )
}

export default Home
