import React, { useState } from "react"
import axios from "../services/axios"

const FormRegister = ({handler}) => {
    const [username, setUsername] = useState("")
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [passwordConfirm, setPasswordConfirm] = useState("")

    const handleSubmit = (e) => {
        e.preventDefault()

        const usernameError = document.querySelector(".username.show__error")
        const emailError = document.querySelector(".email.show__error")
        const passwordError = document.querySelector(".password.show__error")
        const passwordConfirmError = document.querySelector(
            ".passwordConfirm.show__error"
        )

        if (password !== passwordConfirm) {
            passwordConfirmError.innerText =
                "La confirmation du mot de passe est incorrecte"
        } else {
            axios
                .post("/user/register", {
                    username: username,
                    email: email,
                    password: password,
                    passwordConfirm: passwordConfirm,
                })
                .then((res) => {
                    if (res.data.errors) {
                        usernameError.innerText = res.data.errors.username
                        emailError.innerText = res.data.errors.email
                        passwordError.innerText = res.data.errors.password
                        passwordConfirmError.innerText =
                            res.data.errors.passwordConfirm
                    } else {
                        handler()
                    }
                })
                .catch((err) => {
                    console.log(err)
                })
        }
    }

    return (
        <form name="form1" className="box" onSubmit={handleSubmit}>
            <input
                type="text"
                name="username"
                placeholder="Username"
                autoComplete="off"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
            />
            <div className="show__error username"></div>
            <input
                type="email"
                name="email"
                placeholder="Email"
                autoComplete="off"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
            />
            <div className="show__error email"></div>
            <input
                type="password"
                name="password"
                placeholder="Passsword"
                id="pwd"
                autoComplete="off"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
            />
            <div className="show__error password"></div>
            <input
                type="password"
                name="password"
                placeholder="Confirm password"
                id="pwd2"
                autoComplete="off"
                value={passwordConfirm}
                onChange={(e) => setPasswordConfirm(e.target.value)}
            />
            <div className="show__error passwordConfirm"></div>
            <button type="submit" className="btn1">
                Sign Up
            </button>
        </form>
    )
}

export default FormRegister
