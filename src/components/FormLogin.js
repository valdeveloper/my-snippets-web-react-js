import React, { useEffect, useState } from "react"
import axios from "../services/axios"
import { isLogin } from "../services/actions/auth.action"
import { useDispatch } from "react-redux"

const FormLogin = () => {
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")

    const dispatch = useDispatch()

    useEffect(() => {
        let userId = localStorage.getItem("userId")

        axios
            .get(`/user/${userId}`)
            .then((res) => {
                if (res.data._id) {
                    dispatch(isLogin(true))
                }
            })
            .catch((err) => console.log(err))
    }, [])

    const handleSubmit = (e) => {
        e.preventDefault()

        const emailError = document.querySelector(".email.show__error")
        const passwordError = document.querySelector(".password.show__error")

        axios
            .post("/user/login", {
                email: email,
                password: password,
            })
            .then((res) => {
                if (res.data.user) {
                    localStorage.setItem("userId", res.data.user)
                    dispatch(isLogin(true))
                } else if (res.data.errors) {
                    emailError.innerHTML = res.data.errors.email
                    passwordError.innerHTML = res.data.errors.password
                }
            })
            .catch((err) => {
                console.log(err)
            })
    }

    return (
        <form name="form1" className="box" onSubmit={handleSubmit}>
            <input
                type="text"
                name="email"
                placeholder="Email"
                autoComplete="off"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
            />
            <div className="show__error email"></div>
            <input
                type="password"
                name="password"
                placeholder="Passsword"
                id="pwd"
                autoComplete="off"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
            />
            <div className="show__error password"></div>
            <button type="submit" className="btn1">
                Sign In
            </button>
        </form>
    )
}

export default FormLogin
