import axios from 'axios'

axios.defaults.baseURL = "http://localhost:4000/api/v1"//process.env.API_BASE_URL
axios.defaults.withCredentials = true

export default axios