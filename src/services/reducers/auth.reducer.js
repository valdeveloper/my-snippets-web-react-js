import { IS_LOGIN } from "../actions/auth.action"

const initialState = false

export default function authReducer(state = initialState, action) {
    switch (action.type) {
        case IS_LOGIN:
            return action.payload
        default:
            return state
    }
}
