import { GET_ALL_NOTES } from "../actions/note.action"

const initialState = {}

export default function noteReducer(state = initialState, action) {
    switch (action.type) {
        case GET_ALL_NOTES:
            return action.payload
        default:
            return state
    }
}
