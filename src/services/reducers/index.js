import { combineReducers } from "redux"
import userReducer from "./user.reducer"
import noteReducer from "./note.reducer"
import authReducer from "./auth.reducer"

export default combineReducers({
    userReducer,
    noteReducer,
    authReducer
})
