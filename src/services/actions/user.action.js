import axios from "../axios"

export const GET_USER = "GET_USER"

export const getUser = () => {

    let userId = localStorage.getItem("userId")

    return async (dispatch) => {
        return axios
            .get(`/user/${userId}`)
            .then((res) => {
                dispatch({ type: GET_USER, payload: res.data })
            })
            .catch((err) => console.log(err))
    }
}