export const IS_LOGIN = "IS_LOGIN"

export const isLogin = (isLogged) => {
    return (dispatch) => {
        dispatch({ type: IS_LOGIN, payload: isLogged })
    }
}
