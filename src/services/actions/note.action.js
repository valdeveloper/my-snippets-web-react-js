import axios from "../axios"

export const GET_ALL_NOTES = "GET_ALL_NOTES"
export const INSERT_NOTE = "INSERT_NOTE"
export const UPDATE_NOTE = "UPDATE_NOTE"
export const DELETE_NOTE = "DELETE_NOTE"

export const getAllNotes = () => {
    return async (dispatch) => {
        let userId = localStorage.getItem("userId")
        return axios
            .get(`/user/${userId}/note`)
            .then((res) => {
                dispatch({ type: GET_ALL_NOTES, payload: res.data })
            })
            .catch((err) => {
                console.log(err)
            })
    }
}

